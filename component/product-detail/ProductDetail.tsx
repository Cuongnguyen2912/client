import { Button, Container, Grid, Rating } from '@mui/material'
import { notification } from 'antd'
import { useDispatch } from 'react-redux'
import { useSelector } from '../../app/hook'
import { setCart } from '../../app/redux/reducers/auth.slice'
import { apiAddCart } from '../../utils/api/authApi'
import { apiCreateCart, apiUpdateCart } from '../../utils/api/cartApi'
import './style.scss'

const ProductDetail = () => {
  const dispatch = useDispatch();
  const { currentProduct } = useSelector((state) => state.product)
  const { user, cart } = useSelector((state) => state.user)

  const addCart = async () => {
    if (user) {
      const _id = user._id;
      const _cart = [...cart, currentProduct] as any;
      const data = await apiAddCart({ userId: _id, _cart })
      dispatch(setCart(data.cart));
      notification.success({ message: "Sản phẩm đã cho vào giỏ hàng !!" })
    } else {
      notification.warning({ message: "Bạn cần đăng nhập tài khoản !!!" })
    }
  }

  return (
    <div id="product-detail">
      <Container maxWidth="xl">
        {currentProduct && <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <img src={currentProduct?.image || "/assets/img/product-detail.png"} alt="" style={{ width: "100%" }} />
          </Grid>
          <Grid item xs={12} md={6}>
            <h3 className="product-name">{currentProduct?.name || "Plain White Shirt"}</h3>
            <Rating name="product-rating" value={5} readOnly />
            <div className="product-price">{`$${currentProduct?.price}` || "$59.00"}</div>
            <div className="product-desc">{currentProduct?.desc || "Description"}</div>
            <Button className="add-cart" onClick={() => addCart()}>Add to cart</Button>
          </Grid>
        </Grid>}
      </Container>
    </div>
  )
}

export default ProductDetail