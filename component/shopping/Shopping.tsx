import { Button, Container, Grid, InputBase } from '@mui/material'
import { useEffect } from 'react'
import { useSelector } from '../../app/hook'
import Product from '../product/Product'
import './style.scss'

const Shopping = () => {
  const { products } = useSelector((state) => state.product)

  return (
    <div id="shopping">
      <Container maxWidth='xl'>
        <div className="title">Shopping Now</div>
        {/* <div className="search">
          <InputBase placeholder='Article name or keywords...' className='key-search' />
          <Button className='btn-search'>Search</Button>
        </div> */}
        <Grid container spacing={2}>
          {products && products.map((item, index) => (
            <Grid item xs={12} md={6} lg={3} key={index}>
              <Product item={item} />
            </Grid>
          ))}
        </Grid>
        {/* <Button className="load-more">Load More</Button> */}
      </Container>
    </div>
  )
}

export default Shopping