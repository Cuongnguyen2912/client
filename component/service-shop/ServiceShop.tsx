import { Container, Grid } from '@mui/material'
import './style.scss'

const ServiceShop = () => {
  return (
    <div id="service-shop">
      <div className="freeship">
      <Container maxWidth='xl'>
        <Grid container spacing={4}>
          {[1, 2, 3, 4].map((item, index) => (
            <Grid item xs={12} md={6} lg={3} key={index}>
              <img src={`/assets/img/service${item}.png`} alt="" />
            </Grid>
          ))}
        </Grid>
      </Container>
      </div>
      <div className="highlight">
        <Grid container spacing={10} justifyContent="space-between">
          <Grid item xs={12} md={6} lg={7} className="highlight-item">
            <div className="title">peace of mind</div>
            <div className="contene">A one-stop platform for all your fashion needs, hassle-free. Buy with a peace of mind.</div>
          </Grid>
          <Grid item xs={12} md={6} lg={4} className="highlight-item">
            <div className="title">Buy 2 Get 1 Free</div>
            <div className="content">End of season sale. Buy any 2 items of your choice and get 1 free.</div>
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

export default ServiceShop