import { Container, Link } from '@mui/material'
import { useMemo } from 'react'
import { useSelector } from '../../app/hook'
import { useRouter } from 'next/router'
import { notification } from 'antd'
import './style.scss'

const Cart = () => {
  const router = useRouter();
  const { cart, user, quantityCart, cartUni } = useSelector((state) => state.user)
  const totalPrice = useMemo(() => {
    return cart.reduce((pre, cur: any) => pre + cur?.price ?? 0, 0)
  }, [cart])

  return (
    <div id="cart">
      <Container maxWidth="xl">
        <div className="list-product">
          <div className="title">
            <div className="product">Product</div>
            <div className="price">Price</div>
            <div className="quantity">Quantity</div>
            <div className="total">Total</div>
          </div>

          {cartUni && cartUni.map((item: any, index) => (
            <div className="product-detail" key={index}>
              <div className="name">{item?.name}</div>
              <div className="price">${item?.price}</div>
              <div className="quantity">{quantityCart[item._id]}</div>
              <div className="total">${item?.price}</div>
            </div>
          ))}

        </div>
        <div className="cart-total">
          <div className="title-cart-total">Cart Totals</div>
          <div className="content">
            <div className="content-item">
              <div className="title">Subtotal</div>
              <div className="money">${totalPrice}</div>
            </div>
            <div className="content-item">
              <div className="title">Shipping</div>
              <div className="money">Free</div>
            </div>
            <div className="content-item">
              <div className="title">Total</div>
              <div className="money">${totalPrice}</div>
            </div>
          </div>
          <div className="btn-checkout">
            {user ?
              <Link href="/check-out" underline="none">CHECK OUT</Link>
              :
              "CHECKOUT"
            }
          </div>
        </div>
      </Container>
    </div>
  )
}

export default Cart