import { Container } from '@mui/material';
import { Col, Row, Button, Table, Popconfirm, Image } from 'antd'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from '../../app/hook';
import { deleteProduct, getProducts, showModal } from '../../app/redux/reducers/product.slice'
import { apiDeleteProduct } from '../../utils/api/productApi';
import ModalCreateProduct from './ModalCreateProduct';

const CreateProduct = () => {
  const dispatch = useDispatch();
  const { products } = useSelector((state) => state.product)

  useEffect(() => {
    dispatch(getProducts({}));
  }, [])

  return (
    <div className="create-product">
      <Container maxWidth="xl">
        <Row gutter={[8, 8]}>
          <Col span={24} sm={24}>
            <Button onClick={() => dispatch(showModal({ showModal: true, isUpdate: false, currentProduct: {} }))}>Thêm</Button>
            <ModalCreateProduct />

            <Table dataSource={products} size="middle" pagination={{ position: ["bottomRight"] }} scroll={{ x: 768 }}>
              <Table.Column title="Name" dataIndex="name" />
              <Table.Column title="Image" render={(_, product: any) => <Image width={100} src={product.image} />} />
              <Table.Column title="Price" render={(_, product: any) => <span>${product.price}</span>} />
              <Table.Column title="Description" dataIndex="desc" />
              <Table.Column title="Short Description" dataIndex="shortDesc" />
              <Table.Column title="Update" render={(_, product: any) => <Button onClick={() => { dispatch(showModal({ showModal: true, isUpdate: true, currentProduct: product })) }}>Sửa</Button>} />
              <Table.Column title="Delete" render={(_, product: any) =>
                <Popconfirm
                  title="Are you sure to delete this Web Seo?"
                  onConfirm={() => {
                    dispatch(deleteProduct({ _id: product._id }))
                  }}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button>Xóa</Button>
                </Popconfirm>}
              />
            </Table>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default CreateProduct