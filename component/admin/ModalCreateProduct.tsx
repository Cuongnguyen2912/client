import { Button, Col, Form, Input, Modal, notification, Row } from "antd"
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "../../app/hook";
import { createProduct, getProducts, showModal, updateProduct } from "../../app/redux/reducers/product.slice";


const ModalCreateProduct = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { isUpdate, currentProduct } = useSelector((state) => state.product)
  const isVisible = useSelector((state) => state.product.showModal)

  useEffect(() => {
    dispatch(getProducts({}));
  }, [])

  useEffect(() => {
    if (currentProduct) {
      form.setFieldsValue({
        name: currentProduct.name,
        price: currentProduct.price,
        desc: currentProduct.desc,
        shortDesc: currentProduct.shortDesc,
        image: currentProduct.image
      })
    }
  }, [currentProduct])


  const onHandleSubmit = (values: any) => {
    const product = {
      name: values.name,
      price: parseInt(values.price),
      desc: values.desc,
      shortDesc: values.shortDesc,
      image: values.image
    }

    if (isUpdate) {
      dispatch(updateProduct({ product: {_id: currentProduct._id, ...product}, showModal: false }))
    } else {
      dispatch(createProduct({ product, showModal: false }))
    }
    notification.success({ message: `${isUpdate ? "Cập nhật" : "Tạo"} thành công` })
    form.resetFields();
  }

  return (
    <div id="modal-create-product">
      <Modal
        title="Web seo"
        visible={isVisible}
        footer={null}
        onCancel={() => {
          dispatch(showModal({ showModal: false, isUpdate: false, currentProduct: {} }));
          form.resetFields();
        }}
        width="80%"
      >
        <Form
          onFinish={onHandleSubmit}
          form={form}
        >
          <Row gutter={[8, 8]}>
            <Col span={24} md={12} sm={24}>
              <Form.Item name="name" label="Name">
                <Input />
              </Form.Item>
            </Col>
            <Col span={24} md={12} sm={24}>
              <Form.Item name="price" label="Price">
                <Input />
              </Form.Item>
            </Col>
            <Col span={24} md={12} sm={24}>
              <Form.Item name="image" label="Image">
                <Input />
              </Form.Item>
            </Col>
            <Col span={24} md={12} sm={24}>
              <Form.Item name="desc" label="Description">
                <Input.TextArea />
              </Form.Item>
            </Col>
            <Col span={24} md={12} sm={24}>
              <Form.Item name="shortDesc" label="Short Desc">
                <Input.TextArea />
              </Form.Item>
            </Col>
          </Row>

          <Button htmlType="submit" type="primary">
            {isUpdate ? "Update" : "Create"}
          </Button>
        </Form>
      </Modal>
    </div>
  )
}

export default ModalCreateProduct