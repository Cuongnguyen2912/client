import { Container } from '@mui/material'
import { DatePicker, Table } from 'antd'
import moment from 'moment';
import React, { useEffect, useState } from 'react'
import { apiStatisticBill } from '../../utils/api/billApi';

const { RangePicker }: any = DatePicker;

const StatisticBill = () => {
  const [total, setTotal] = useState(0);

  useEffect(() => {
    apiStatisticBill().then((res) => setTotal(res))
  }, [])

  const onChange = (value, dateString) => {
    apiStatisticBill(moment(value[0]).valueOf(), moment(value[1]).valueOf()).then((res) => setTotal(res))
  }


  return (
    <div id="statistic-bill">
      <Container maxWidth="xl">
        {/* <RangePicker
          onChange={onChange}
        /> */}
        <div className="total-money">Revenue statistics : ${total}</div>
      </Container>
    </div>
  )
}

export default StatisticBill