import { Container, Grid, Link } from '@mui/material'
import FacebookIcon from '@mui/icons-material/Facebook'
import YouTubeIcon from '@mui/icons-material/YouTube'
import './style.scss'

const Footer = () => {
  return (
    <div id="footer">
      <Container maxWidth='xl'>
        <Grid container spacing={4}>
          <Grid item xs={6} md={3}>
            <Link href="/"><img src="/assets/icon/logo.png" alt="" /></Link>
          </Grid>
          <Grid item xs={6} md={3}>
            <div><Link href="/" underline="none">Home</Link></div>
            <div><Link href="/shopping-now" underline="none">Shopping Now</Link></div>
          </Grid>
          <Grid item xs={6} md={3}>
            <div><Link href="#" underline="none">About</Link></div>
            <div><Link href="#" underline="none">Contact</Link></div>
          </Grid>
          <Grid item xs={6} md={3}>
            <div><Link href="#" underline="none"><FacebookIcon /></Link></div>
            <div><Link href="#" underline="none"><YouTubeIcon /></Link></div>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Footer