import './style.scss'
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { setCurrentProduct } from '../../app/redux/reducers/product.slice';
import { Link } from '@mui/material';


const Product = (props: { item?: any }) => {
  const { item } = props;
  const router = useRouter();
  const dispatch = useDispatch();

  return (
    <div id="product" onClick={() => {
      dispatch(setCurrentProduct(item))
    }}>
      <Link href={`/product/${item?._id}`} underline="none">
        <img src={item?.image || "/assets/img/product1.png"} alt="" className="img-product" />
        <div className="name-product">{item?.name ?? "Plain White Shirt"}</div>
        <div className="price-product">{`$${item?.price}` ?? "$29.00"}</div>
      </Link>
    </div >
  )
}

export default Product