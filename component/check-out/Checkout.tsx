import { Container, Button } from '@mui/material'
import React, { useMemo } from 'react'
import { useSelector } from '../../app/hook'
import { useForm } from "react-hook-form";
import './style.scss'
import { notification } from 'antd';
import { apiAddCart } from '../../utils/api/authApi';
import { useDispatch } from 'react-redux';
import { setCart } from '../../app/redux/reducers/auth.slice';
import { apiCreateBill } from '../../utils/api/billApi';
import moment from 'moment';

const Checkout = () => {
  const { register, handleSubmit, reset, watch, formState: { errors } } = useForm();
  const { cart, user } = useSelector((state) => state.user)
  const dispatch = useDispatch();
  const totalPrice = useMemo(() => {
    return cart.reduce((pre, cur: any) => pre + cur?.price, 0)
  }, [cart])

  const handleCheckout = async (values: any) => {
    const _id = user._id
    notification.success({ message: "Tạo đơn hàng thành công !!" })
    const data = await apiAddCart({ userId: _id, _cart: [] })
    await apiCreateBill(moment().valueOf(), totalPrice )
    dispatch(setCart(data.cart));
    reset()
    window.location.href = "/"
  }

  return (
    <div id="check-out">
      <Container maxWidth="xl">
        <form onSubmit={handleSubmit(handleCheckout)}>
          <div className="bill-detail">
            <div className="title">Billing details</div>
            <div className="input-cpt">
              <div className="label-wrapper">
                <label className="label">Full Name (*)</label>
              </div>
              <input required type="text" className="input-field" {...register('name')} />
            </div>
            <div className="input-cpt">
              <div className="label-wrapper">
                <label className="label">Phone (*)</label>
              </div>
              <input required type="text" className="input-field" {...register('phone')} />
            </div>
            <div className="input-cpt">
              <div className="label-wrapper">
                <label className="label">Address (*)</label>
              </div>
              <input required type="text" className="input-field" {...register('address')} />
            </div>
          </div>
          <div className="your-order">
            <div className="title">Your order</div>
            <div className="total">
              <div className="title-total">Total</div>
              <div className="total-price">${totalPrice}</div>
            </div>
          </div>
          <Button className="btn-payment" type="submit" >PAYMENT</Button>
        </form>
      </Container>
    </div>
  )
}

export default Checkout