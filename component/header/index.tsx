import { Badge, Container, Grid, IconButton, Link, Menu, MenuItem, Typography } from '@mui/material'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import LocalMallIcon from '@mui/icons-material/LocalMall';
import MenuIcon from '@mui/icons-material/Menu';
import './style.scss';
import { useEffect, useMemo, useState } from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { themeOptions } from '../../styles/themes';
import { useSelector } from '../../app/hook';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import Cookie from 'js-cookie'

const Header = () => {
  const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);
  const theme = useTheme();
  const dispatch = useDispatch();
  const matchesMenuDesktop = useMediaQuery(theme.breakpoints.up(themeOptions.breakpoints.values.md));

  const { user, cart } = useSelector((state) => state.user);
  const router = useRouter();

  const handleLogout = () => {
    Cookie.remove("user");
    window.location.href = "/";
  }

  return (
    <div id="header">
      <Container maxWidth="xl">
        <Grid container spacing={2} alignItems="center" justifyContent="space-between">
          <Grid item xs={10} md={6} lg={3}>
            <Link href="/" underline="none"><img src="/assets/icon/logo.png" alt="" style={{ cursor: "pointer" }} /></Link>
          </Grid>
          <Grid item xs={2} md={6} lg={9} className="navigation">
            {matchesMenuDesktop &&
              <Grid container spacing={2} alignItems="center" justifyContent="space-between">
                <Grid item xs={6} style={{ display: "flex" }}>
                  <Link href="/" underline="none"  style={{ paddingRight: "10px" }}>Home</Link>
                  <Link href="/shopping-now" underline="none">Shopping Now</Link>
                </Grid>
                <Grid item xs={6}>
                  <Grid container spacing={2} textAlign="right" alignItems="start">
                    <Grid item xs={11}>
                      {user ?
                        <div style={{ display: "flex", justifyContent: "end", gap: "10px" }}>
                          <PersonOutlineIcon style={{ cursor: "pointer" }} /> <span onClick={() => handleLogout()} style={{ cursor: "pointer" }}>Logout</span>
                        </div>
                        :
                        <Link href="/login" underline="none">Login</Link>
                      }
                    </Grid>
                    <Grid item xs={1}>
                      {user &&
                        <Badge badgeContent={cart?.length ?? 0} color="primary">

                          <Link href="/cart" underline="none"><LocalMallIcon /></Link>
                        </Badge>
                      }
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            }
            {!matchesMenuDesktop &&
              <div>
                <IconButton size="large" aria-controls="menu-appbar" aria-haspopup="true" sx={{ color: "#000", textAlign: "right" }}
                  onClick={(event) => setAnchorElNav(event.currentTarget)}
                >
                  <MenuIcon />
                </IconButton>
                <Menu id="menu-appbar" anchorEl={anchorElNav} keepMounted open={Boolean(anchorElNav)}
                  anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                  transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                  onClose={() => setAnchorElNav(null)}
                  sx={{ display: { xs: 'block', md: 'none' }, }}
                >
                  <MenuItem onClick={() => setAnchorElNav(null)}>
                    <Link href="/" underline="none">Home</Link>
                  </MenuItem>
                  <MenuItem onClick={() => setAnchorElNav(null)}>
                    <Link href="/shopping-now" underline="none">Shopping Now</Link>
                  </MenuItem>
                  <MenuItem onClick={() => setAnchorElNav(null)}>
                    {user ?
                      <>
                        <PersonOutlineIcon style={{ cursor: "pointer" }} /> <span onClick={() => handleLogout()} style={{ cursor: "pointer" }}>Logout</span>
                      </>
                      :
                      <Link href="/login" underline="none">Login</Link>
                    }
                  </MenuItem>
                  {user &&
                    <MenuItem onClick={() => setAnchorElNav(null)}>
                      <Link href="/cart" underline="none"><LocalMallIcon /></Link>

                    </MenuItem>
                  }
                </Menu>
              </div>
            }
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Header