import { Container, Grid } from '@mui/material'
import { useSelector } from '../../app/hook'
import Product from '../product/Product'
import './style.scss'

const TopSeller = () => {
  const { products } = useSelector((state) => state.product)

  return (
    <div id="top-seller">
      <Container maxWidth="xl">
        <h2 className="title-seo">Top Sellers</h2>
        <h3 className="content-seo">Browse our top-selling products</h3>
        <Grid container spacing={2}>
          {products && products.slice(0, 4).map((item, index) => (
            <Grid item xs={12} md={6} lg={3} key={index}>
              <Product item={item} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  )
}

export default TopSeller