import { Container, Grid } from '@mui/material'
import { useSelector } from '../../app/hook'
import Product from '../product/Product'
import './style.scss'

const NewProduct = () => {
  const { products } = useSelector((state) => state.product)
  return (
    <div id="new-product">
      <Container maxWidth="xl">
        <h2 className="title-seo">Discover NEW Arrivals</h2>
        <h3 className="content-seo">Recently added clothes!</h3>
        <Grid container spacing={2}>
          {products && products.slice(4, 8).map((item, index) => (
            <Grid item xs={12} md={6} lg={3} key={index}>
              <Product item={item} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  )
}

export default NewProduct