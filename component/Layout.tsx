import Head from "next/head";
import { PropsWithChildren } from "react";
import 'antd/dist/antd.css';

export type LayoutProps = {
  slug?: string;
  title?: string;
  keywords?: string;
  description?: string;
  favicon?: string;
  siteAddress?: string;
  metaRobot?: number;
}

const Layout = (props: PropsWithChildren<LayoutProps>) => {
  const { children,
    title,
    keywords,
    description,
    favicon,
    slug,
    siteAddress,
    metaRobot
  } = props;

  const canonical = !!slug ? `${siteAddress || 'http://localhost:3000'}/${slug === "/" ? "" : slug}` : '';

  return (<>
    <Head>
      <title>Fashion TTCS</title>
      <meta name="keywords" content={keywords || ''} />
      <meta name="description" content={description || ''} />
      <meta name="title" content={title} />
      {typeof metaRobot !== "undefined" && <meta name="robots" />}
      <link rel="shortcut icon" href={favicon || '/favicon.ico'} />
      {!!canonical && <link rel="canonical" href={canonical} />}
    </Head>
    {children}

  </>)
}

export default Layout;