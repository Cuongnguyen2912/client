import { Product } from '../../app/redux/reducers/product.slice';
import { get, getEndpoint } from '../fetcher';
import { post } from "../fetcher"

export const apiLogin = async (email: string, password: string) => {
  const { data, error } = await post({ endpoint: getEndpoint("/login"), body: { email, password } });
  return error ? null : data;
}

export const apiRegister = async (email: string, password: string, name: string) => {
  const { data, error } = await post({ endpoint: getEndpoint("/register"), body: { email, password, name } });
  return error ? null : data;
}

export const apiLogout = async () => {
  const { data, error } = await post({ endpoint: getEndpoint("/logout"), body: {} });
  return error ? null : data;
}

export const apiRefreshToken = async () => {
  const { data, error } = await get({ endpoint: getEndpoint("/refresh_token") });
  return error ? null : data;
}

export const apiAddCart = async (args: { userId: string, _cart }) => {
  const { userId, _cart } = args;
  const { data, error } = await post({ endpoint: getEndpoint("/add-cart"), body: { userId, _cart } });
  return error ? null : data;
}

export const apiGetUserById = async (_id: string) => {
  const { data, error } = await post({ endpoint: getEndpoint("/get-user-by-id"), body: { _id } });
  return error ? null : data;
}