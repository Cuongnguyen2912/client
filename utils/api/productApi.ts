import { getEndpoint, post } from "../fetcher";

export const apiGetProducts = async () => {
  const { data, error } = await post({ endpoint: getEndpoint("/get-products"), body: {} });
  return error ? null : data;
}

export const apiGetProduct = async (_id: string) => {
  const { data, error } = await post({ endpoint: getEndpoint(`/get-product`), body: { _id } });
  return error ? null : data;
}

export const apiCreateProduct = async (product: any) => {
  const { data, error } = await post({ endpoint: getEndpoint(`/create-product`), body: { product } });
  return error ? null : data;
}

export const apiUpdateProduct = async (product: any) => {
  const { _id, ...args } = product
  const { data, error } = await post({ endpoint: getEndpoint(`/update-product`), body: { _id, product: { ...args } } });
  return error ? null : data;
}

export const apiDeleteProduct = async (_id: string) => {
  const { data, error } = await post({ endpoint: getEndpoint("/delete-product"), body: { _id } });
  return error ? null : data;
}