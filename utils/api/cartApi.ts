import { getEndpoint, post } from "../fetcher";

export const apiUpdateCart = async (cart: {userId: string, products: any}) => {
  const { data, error } = await post({ endpoint: getEndpoint("/update-cart"), body: { cart } });
  return error ? null : data;
}

export const apiCreateCart = async (cart: { userId: string, products: any }) => {
  const { data, error } = await post({ endpoint: getEndpoint("/create-cart"), body: { cart } });
  return error ? null : data;
}