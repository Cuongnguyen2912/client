import { getEndpoint, post } from "../fetcher";

export const apiCreateBill = async (time: number, money: number) => {
  const { data, error } = await post({ endpoint: getEndpoint("/create-bill"), body: { time, money } });
  return error ? null : data;
}

export const apiStatisticBill = async (startTime?: number, endTime?: number) => {
  const { data, error } = await post({ endpoint: getEndpoint("/statistic-bill"), body: { startTime, endTime } });
  return error ? null : data;
}