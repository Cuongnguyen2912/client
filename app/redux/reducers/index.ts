import { combineReducers } from "redux";
import counterReducer from "./couter.reducers";
import UserReducer from './auth.slice';
import ProductReducer from './product.slice';

export const rootReducers = combineReducers({
  counters: counterReducer,
  user: UserReducer,
  product: ProductReducer
});