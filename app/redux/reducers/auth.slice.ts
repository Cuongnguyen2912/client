import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { apiLogin, apiRefreshToken, apiRegister } from "../../../utils/api/authApi"
import { Product } from "./product.slice"
import Cookie from 'js-cookie'
import lodash from 'lodash'

export type UserReducer = {
  user: any,
  cart?: [],
  quantityCart?: {},
  cartUni?: any
}

const initialState: UserReducer = {
  user: null,
  cart: [],
  quantityCart: {},
  cartUni: []
}

export const login = createAsyncThunk("auth/login", async (args: { email: string, password: string }) => {
  const { email, password } = args;
  const data = await apiLogin(email, password);
  Cookie.set('user', JSON.stringify(data._user._id));
  return data;
})

export const registerRT = createAsyncThunk("auth/registerRT", async (args: { email: string, password: string, name: string }) => {
  const { email, password, name } = args;
  const data = await apiRegister(email, password, name);
  Cookie.set('user', JSON.stringify(data._user._id));
  return data;
})

export const refreshToken = createAsyncThunk("auth/refreshToken", async () => {
  if (localStorage.getItem("auth")) {
    const data = await apiRefreshToken();
    return data;
  }
  return null
})

const convertCart = (cart: Product[]) => {
  const _cart = {};
  cart && cart.map((item: Product) => {
    if (_cart[item._id]) {
      _cart[item._id]++;
    } else {
      _cart[item._id] = 1;
    }
  })
  return _cart
}

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setCart: (state, action: PayloadAction<any>) => {
      state.cart = action.payload
      state.quantityCart = convertCart(action.payload)
      state.cartUni = lodash.uniqBy(action.payload, '_id')
    },
    setUser: (state, action: PayloadAction<any>) => {
      state.user = action.payload
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.fulfilled, (state, action) => {
        state.user = action.payload._user
        state.cart = action.payload._user.cart
        state.quantityCart = convertCart(action.payload._user.cart)
        state.cartUni = lodash.uniqBy(action.payload._user.cart, '_id')

      })
      .addCase(registerRT.fulfilled, (state, action) => {
        state.user = action.payload._user
        state.cart = action.payload._user.cart
        state.quantityCart = convertCart(action.payload._user.cart)
        state.cartUni = lodash.uniqBy(action.payload._user.cart, '_id')
      })
      .addCase(refreshToken.fulfilled, (state, action) => {
        state.user = action.payload._user
        state.cart = action.payload._user.cart
        state.quantityCart = convertCart(action.payload._user.cart)
        state.cartUni = lodash.uniqBy(action.payload._user.cart, '_id')
      })
  }
})

export const { setCart, setUser } = userSlice.actions;
export default userSlice.reducer;