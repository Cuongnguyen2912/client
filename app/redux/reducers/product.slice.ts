import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { apiCreateProduct, apiDeleteProduct, apiGetProduct, apiGetProducts, apiUpdateProduct } from "../../../utils/api/productApi"

export type Product = {
  _id?: string
  name?: string,
  price?: number,
  image?: string,
  desc?: string,
  shortDesc?: string,
  quantity?: number
}

export type ProductReducer = {
  products: Product[],
  currentProduct: Product,
  showModal: boolean,
  isUpdate: boolean
}

const initialState: ProductReducer = {
  products: [],
  currentProduct: null,
  showModal: false,
  isUpdate: false
}

export const getProducts = createAsyncThunk("product/getProducts", async (args: {}) => {
  const data = await apiGetProducts();
  return data;
})

export const getProduct = createAsyncThunk("product/getProduct", async (args: { _id: string }) => {
  const { _id } = args;
  const data = await apiGetProduct(_id);
  return data;
})

export const createProduct = createAsyncThunk("product/createProduct", async (args: { product: any, showModal: boolean }) => {
  const { product, showModal } = args;
  const data = await apiCreateProduct(product);
  return {
    data,
    showModal
  };
})

export const updateProduct = createAsyncThunk("product/updateProduct", async (args: { product: any, showModal: boolean }) => {
  const { product, showModal } = args;
  const data = await apiUpdateProduct(product);
  return {
    data,
    showModal
  };
})

export const deleteProduct = createAsyncThunk("product/deleteProduct", async (args: { _id: string }) => {
  const { _id } = args;
  const data = await apiDeleteProduct(_id);
  return _id;
})

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    setCurrentProduct: (state, action: PayloadAction<Product>) => {
      state.currentProduct = action.payload
    },
    setProducts: (state, action: PayloadAction<any>) => {
      state.products = action.payload
    },
    showModal: (state, action: PayloadAction<{ showModal: boolean, isUpdate?: boolean, currentProduct?: any }>) => {
      state.showModal = action.payload.showModal
      state.isUpdate = action.payload.isUpdate
      state.currentProduct = action.payload.currentProduct
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getProducts.fulfilled, (state, action) => {
        state.products = action.payload
      })
      .addCase(getProduct.fulfilled, (state, action) => {
        state.currentProduct = action.payload
      })
      .addCase(createProduct.fulfilled, (state, action) => {
        state.products = [...state.products, action.payload.data]
        state.showModal = action.payload.showModal
      })
      .addCase(updateProduct.fulfilled, (state, action) => {
        state.products = state.products.map(item => {
          if (item._id === action.payload.data._id) return action.payload.data
          return item
        })
        state.showModal = action.payload.showModal
      })
      .addCase(deleteProduct.fulfilled, (state, action) => {
        state.products = state.products.filter(item => item._id !== action.payload)
      })
  }
})

export const { setCurrentProduct, setProducts, showModal } = productSlice.actions;
export default productSlice.reducer;