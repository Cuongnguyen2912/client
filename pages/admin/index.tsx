import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import CreateProduct from '../../component/admin/CreateProduct'
import StatisticBill from '../../component/admin/StatisticBill'
import Header from '../../component/header'
import Layout from '../../component/Layout'
import Cookie from 'js-cookie'
import { setCart, setUser } from '../../app/redux/reducers/auth.slice'
import { useRouter } from "next/router";
import { apiGetUserById } from '../../utils/api/authApi'
import { wrapper } from '../../app/store'

const index = (props: any) => {
  const { data } = props;
  const dispatch = useDispatch();
  const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;

  const router = useRouter();
  useEffect(() => {
    apiGetUserById(_user).then((res) => {
      dispatch(setUser(res))
      dispatch(setCart(res.cart))
      if (res?.role !== 1) {
        router.replace('/')
      }
    })
  }, [])
  

  return (
    <Layout>
      <Header />
      <StatisticBill />
      <CreateProduct />
    </Layout>
  )
}

export default index