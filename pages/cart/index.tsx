import Cart from '../../component/cart/Cart'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header'
import Layout from '../../component/Layout'
import Cookie from 'js-cookie'
import { useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { setCart, setUser } from '../../app/redux/reducers/auth.slice'
import { wrapper } from '../../app/store'
import { apiGetUserById } from '../../utils/api/authApi'

const index = (props: any) => {

  const dispatch = useDispatch();

  useEffect(() => {
    const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;
    if (_user) {
      apiGetUserById(_user).then((res) => {
        dispatch(setUser(res))
        dispatch(setCart(res.cart))
      })
    }
  }, [])

  return (
    <Layout>
      <Header />
      <Cart />
      <Footer />
    </Layout>
  )
}

export default index