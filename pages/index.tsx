import axios from "axios";
import { PropsWithoutRef, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "../app/hook";
import { refreshToken, setCart, setUser } from "../app/redux/reducers/auth.slice";
import { getProducts, setProducts } from "../app/redux/reducers/product.slice";
import { wrapper } from "../app/store";
import Banner from "../component/banner-section/Banner";
import Footer from "../component/footer/Footer";
import Header from "../component/header";
import Layout from "../component/Layout";
import NewProduct from "../component/new-product/NewProduct";
import ServiceShop from "../component/service-shop/ServiceShop";
import TopSeller from "../component/top-seller/TopSeller";
import { apiGetUserById, apiRefreshToken } from "../utils/api/authApi";
import { apiGetProducts } from "../utils/api/productApi";
import Cookie from 'js-cookie'

const IndexPage = (props: PropsWithoutRef<any>) => {
  const { products } = props;
  const dispatch = useDispatch();
  const { user, cart } = useSelector((state) => state.user)

  useEffect(() => {
    dispatch(setProducts(products))
    const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;
    if (_user) {
      apiGetUserById(_user).then((res) => {
        dispatch(setUser(res))
        dispatch(setCart(res.cart))
      })
    }
  }, [])

  return (<Layout>
    <Header />
    <Banner />
    <NewProduct />
    <ServiceShop />
    <TopSeller />
    <Footer />
  </Layout>);
}

export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {
  const products = await apiGetProducts();
  store.dispatch(setProducts(products));

  // const data = await apiRefreshToken();
  // console.log('dd', data)
  // store.dispatch(setUser(data));

  // store.dispatch(refreshToken({}))

  // const data = await apiRefreshToken();

  return {
    props: {
      products
    }
  }
})

export default IndexPage;