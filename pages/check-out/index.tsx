import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import Checkout from '../../component/check-out/Checkout'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header'
import Layout from '../../component/Layout'
import Cookie from 'js-cookie'
import { setCart, setUser } from '../../app/redux/reducers/auth.slice'
import { apiGetUserById } from '../../utils/api/authApi'

const index = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;
    if (_user) {
      apiGetUserById(_user).then((res) => {
        dispatch(setUser(res))
        dispatch(setCart(res.cart))
      })
    }
  }, [])

  return (
    <Layout>
      <Header />
      <Checkout />
      <Footer />
    </Layout>
  )
}

export default index