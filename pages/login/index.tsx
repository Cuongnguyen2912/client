import React, { useState } from 'react'
import Layout from '../../component/Layout'
import { useRouter } from 'next/router';
import { useForm } from "react-hook-form";
import './style.scss';
import { Checkbox, Container, Link } from '@mui/material';
import Header from '../../component/header';
import { apiLogin } from '../../utils/api/authApi';
import { login } from '../../app/redux/reducers/auth.slice';
import { useDispatch } from 'react-redux';
import { useSelector } from '../../app/hook';
import Cookie from 'js-cookie';

const index = () => {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [showPass, setShowPass] = useState(false);
  const router = useRouter();
  
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);

  const handleLogin = (values: any) => {
    const { email, password } = values;
    dispatch(login({ email, password }));
    router.replace('/')
  }

  return (
    <Layout>
      <Header />
      <Container maxWidth="xl">
        <div className="login">
          <div className="">
            <form className="login-form" onSubmit={handleSubmit(handleLogin)}>
              <div className="input-cpt">
                <div className="label-wrapper">
                  <label className="label">Email (*)</label>
                </div>
                <input required type="email" className="input-field" {...register("email")} />
              </div>
              <div className="input-cpt">
                <div className="label-wrapper">
                  <label className="label">Password (*)</label>
                </div>
                <input required type={showPass ? "text" : "password"} className="input-field" {...register("password")} />
              </div>
              <div className="show-password" style={{ width: "100%" }}>
                <Checkbox onChange={(e) => setShowPass(e.target.checked)} /> Show password
              </div>
              <button className="btn-submit" type="submit">Login</button>
              <p>You don't have an account? <Link href="/register" underline="none">Register</Link></p>
            </form>
          </div>
        </div>
      </Container>
    </Layout>

  )
}

export default index