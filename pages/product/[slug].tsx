import { PropsWithoutRef, useEffect } from 'react'
import { wrapper } from '../../app/store'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header'
import Layout from '../../component/Layout'
import ProductDetail from '../../component/product-detail/ProductDetail'
import { useRouter } from 'next/router';
import { apiGetProduct } from '../../utils/api/productApi'
import { useDispatch } from 'react-redux'
import { setCurrentProduct } from '../../app/redux/reducers/product.slice'
import { setCart, setUser } from '../../app/redux/reducers/auth.slice'
import Cookie from 'js-cookie'
import { useSelector } from '../../app/hook'
import { apiGetUserById } from '../../utils/api/authApi'


const Product = (props: PropsWithoutRef<any>) => {
  const { data } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setCurrentProduct(data))
    const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;
    if (_user) {
      apiGetUserById(_user).then((res) => {
        dispatch(setUser(res))
        dispatch(setCart(res.cart))
      })
    }
  }, [props])

  return (
    <Layout>
      <Header />
      <ProductDetail />
      <Footer />
    </Layout>
  )
}


export const getServerSideProps = wrapper.getServerSideProps(async ({ store, query }) => {
  const slug = query.slug as string;
  const data = await apiGetProduct(slug);
  store.dispatch(setCurrentProduct(data))
  return {
    props: {
      data
    }
  }

})

export default Product