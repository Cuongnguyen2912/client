import { PropsWithoutRef, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { setProducts } from '../../app/redux/reducers/product.slice'
import { wrapper } from '../../app/store'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header'
import Layout from '../../component/Layout'
import Shopping from '../../component/shopping/Shopping'
import { apiGetProducts } from '../../utils/api/productApi'
import Cookie from 'js-cookie'
import { setCart, setUser } from '../../app/redux/reducers/auth.slice'
import { apiGetUserById } from '../../utils/api/authApi'

const ShoppingNow = (props: PropsWithoutRef<{}>) => {

  const dispatch = useDispatch();

  useEffect(() => {
    const _user = Cookie.get("user") ? JSON.parse(Cookie.get("user") || "") : null;
    if (_user) {
      apiGetUserById(_user).then((res) => {
        dispatch(setUser(res))
        dispatch(setCart(res.cart))
      })
    }
  }, [])
  return (
    <Layout>
      <Header />
      <Shopping />
      <Footer />
    </Layout>
  )
}

export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {
  const products = await apiGetProducts();
  store.dispatch(setProducts(products));
  return {
    props: {
      products
    }
  }
})

export default ShoppingNow