import { Container } from "@mui/material";
import ErrorView from "../component/error/ErrorView"

const ErrorNotFound = () => {
  return <Container maxWidth="xl">
    <ErrorView message="Not Found" />
  </Container>
}

export default ErrorNotFound