const withCSS = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");
const withImages = require("next-images");
const withFonts = require("next-fonts");
const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([withCSS, withSass, withImages, withFonts, {
  exportPathMap: (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) => { },
  // includePaths: [path.join(__dirname, 'style')],
  typescript: {
    ignoreBuildErrors: true
  },
}]);